# -*- coding: utf-8 -*-

#  Licensed under the Apache License, Version 2.0 (the "License"); you may
#  not use this file except in compliance with the License. You may obtain
#  a copy of the License at
#
#       https://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#  License for the specific language governing permissions and limitations
#  under the License.

from __future__ import unicode_literals

import os
import sys
from argparse import ArgumentParser

from flask import Flask, request, abort
from linebot import (
    LineBotApi, WebhookParser
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage,
    SourceUser, SourceGroup, SourceRoom,
)

app = Flask(__name__)

# get channel_secret and channel_access_token from your environment variable
channel_secret = os.getenv('LINE_CHANNEL_SECRET', None)
channel_access_token = os.getenv('LINE_CHANNEL_ACCESS_TOKEN', None)

if channel_secret is None:
    print('Specify LINE_CHANNEL_SECRET as environment variable.')
    sys.exit(1)
if channel_access_token is None:
    print('Specify LINE_CHANNEL_ACCESS_TOKEN as environment variable.')
    sys.exit(1)

line_bot_api = LineBotApi(channel_access_token)
parser = WebhookParser(channel_secret)


@app.route("/callback", methods=['POST'])
def callback():
    signature = request.headers['X-Line-Signature']

    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)

    # parse webhook body
    try:
        events = parser.parse(body, signature)
    except InvalidSignatureError:
        abort(400)

    # if event is MessageEvent and message is TextMessage, then echo text
    for event in events:
        if not isinstance(event, MessageEvent):
            continue
#        if not isinstance(event.message, TextMessage):
#            continue
        
        if isinstance(event.source, SourceUser):
            profile = line_bot_api.get_profile(event.source.user_id)
            try:
                line_bot_api.reply_message(
                    event.reply_token,
                    [
                        TextSendMessage(
                            text= \
                            '您的姓名: ' + profile.display_name + '\n' + \
                            '您的頭貼: ' + profile.picture_url + '\n' + \
                            '您的使用者名稱: ' + profile.user_id + '\n' + \
                            '您的訊息狀態: ' + str(profile.status_message) + '\n' + \
                            '您的訊息編號: ' + event.message.id + '\n' + \
                            '您的訊息內容: ' + event.message.text + '\n' + \
                            '您的訊息類型: ' + event.message.type + '\n' + \
                            '您的訊息來源: ' + event.source.type + '\n' + \
                            '您的時間標記: ' + str(event.timestamp) + '\n' + \
                            '您的訊息類型: ' + event.type + '\n'
                        )
                    ]
                )
            except:
                line_bot_api.reply_message(
                    event.reply_token,
                    [
                        TextSendMessage(
                            text= '請傳文字訊息以便辨別，謝謝。'
                        )
                    ]
                )  
        elif isinstance(event.source, SourceGroup):
            profile = line_bot_api.get_group_member_profile(event.source.group_id, event.source.user_id)
            try:
                line_bot_api.reply_message(
                    event.reply_token,
                    [
                        TextSendMessage(
                            text= \
                            '您的姓名: ' + profile.display_name + '\n' + \
                            '您的頭貼: ' + profile.picture_url + '\n' + \
                            '您的使用者名稱: ' + profile.user_id + '\n' + \
                            '您的訊息狀態: ' + str(profile.status_message) + '\n' + \
                            '您的訊息編號: ' + event.message.id + '\n' + \
                            '您的訊息內容: ' + event.message.text + '\n' + \
                            '您的訊息類型: ' + event.message.type + '\n' + \
                            '您的訊息來源: ' + event.source.type + '\n' + \
                            '您的群組名稱: ' + event.source.group_id + '\n' + \
                            '您的時間標記: ' + str(event.timestamp) + '\n' + \
                            '您的訊息類型: ' + event.type + '\n'
                        )
                    ]
                )
            except:
                line_bot_api.reply_message(
                    event.reply_token,
                    [
                        TextSendMessage(
                            text= '請傳文字訊息以便辨別，謝謝。'
                        )
                    ]
                )  
        elif isinstance(event.source, SourceRoom):
            profile = line_bot_api.get_room_member_profile(event.source.room_id, event.source.user_id)
            try:
                line_bot_api.reply_message(
                    event.reply_token,
                    [
                        TextSendMessage(
                            text= \
                            '您的姓名: ' + profile.display_name + '\n' + \
                            '您的頭貼: ' + profile.picture_url + '\n' + \
                            '您的使用者名稱: ' + profile.user_id + '\n' + \
                            '您的訊息狀態: ' + str(profile.status_message) + '\n' + \
                            '您的訊息編號: ' + event.message.id + '\n' + \
                            '您的訊息內容: ' + event.message.text + '\n' + \
                            '您的訊息類型: ' + event.message.type + '\n' + \
                            '您的訊息來源: ' + event.source.type + '\n' + \
                            '您的房間名稱: ' + event.source.room_id + '\n' + \
                            '您的時間標記: ' + str(event.timestamp) + '\n' + \
                            '您的訊息類型: ' + event.type + '\n'
                        )
                    ]
                )
            except:
                line_bot_api.reply_message(
                    event.reply_token,
                    [
                        TextSendMessage(
                            text= '請傳文字訊息以便辨別，謝謝。'
                        )
                    ]
                )  

    return 'OK'


if __name__ == "__main__":
    arg_parser = ArgumentParser(
        usage='Usage: python ' + __file__ + ' [--port <port>] [--help]'
    )
    arg_parser.add_argument('-p', '--port', type=int, default=5000, help='port')
    arg_parser.add_argument('-d', '--debug', default=False, help='debug')
    options = arg_parser.parse_args()

    app.run(debug=options.debug, port=options.port)
